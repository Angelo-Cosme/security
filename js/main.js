
jQuery(function ($) {
// Tooltip
$('[data-bs-toggle="tooltip"]').tooltip();

// Service-details-carousel
$(".service-details-carousel-item:nth-child(1)").show();
$(".service-tab-list").click(function() {
    var tab_modal = $(this).attr("data-service-tab-item");
    $(this).addClass("service-tab-active").siblings().removeClass("service-tab-active");
    $(".service-details-carousel-item[data-service-tab-details=" +tab_modal+ "]").slideDown(600).siblings().slideUp(500);
})

// Choose-label-list
$(".choose-label-list").on("click", function() {
    var self = $(this);
    var self_modal = $(this).attr("data-choose-label");
    self.addClass("active").siblings().removeClass("active");
    $(".choose-details-item[data-choose-details="+ self_modal +"]").addClass("active").siblings().removeClass("active");
})

});




$(document).ready(function(){
    $('.owl-carousel').owlCarousel();
  });
  

  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:30,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:4,
            nav:false
        },
        1000:{
            items:6,
            nav:true,
            loop:false
        }
    }
})
